const pull = require('pull-stream')
const { GraphQLError } = require('graphql')

module.exports = function CountProfiles (sbot) {
  return function countProfiles (cb) {
    pull(
      sbot.messagesByType({ type: 'profile/person' }),
      pull.filter(m => {
        const { tangles } = m.value.content
        if (!tangles) return false
        if (!tangles.profile) return false
        return tangles.profile.root === null
      }),
      pull.collect((err, msgs) => {
        if (err) cb(new GraphQLError(err))
        cb(null, msgs.length)
      })
    )
  }
}
