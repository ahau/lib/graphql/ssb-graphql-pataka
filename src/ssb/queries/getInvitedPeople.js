const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { GraphQLError } = require('graphql')

module.exports = function GetInvitedPeople (sbot, getProfile) {
  return function getInvitedPeople (cb) {
    const profileId = sbot.id

    sbot.friends.hops(profileId, (err, friends) => {
      if (err) return cb(new GraphQLError(err))

      pull(
        pull.keys(friends),
        pull.filter(friend => friend !== profileId),
        paraMap(
          (feedId, cb) => sbot.profile.findByFeedId(feedId, { getProfile }, cb),
          4
        ),
        pull.map(p => {
          if (p.private.length) return p.private[0]
          if (p.public.length) return p.public[0]
          return null
        }),
        pull.filter(Boolean),
        pull.collect((err, profiles) => {
          if (err) cb(new GraphQLError(err))
          else cb(null, profiles)
        })
      )
    })
  }
}
