const { promisify: p } = require('util')

const GetInvitedPeople = require('./queries/getInvitedPeople')
const CountCommunities = require('./queries/countCommunities')
const CountProfiles = require('./queries/countProfiles')
const CountWhakapapaViews = require('./queries/countWhakapapaViews')

module.exports = function (sbot, externalGetters) {
  const { getProfile } = externalGetters
  if (!sbot) throw new Error('invalid sbot')
  // if (sbot.friends) throw new Error('Needs ssb-friends plugin')

  const getInvitedPeople = GetInvitedPeople(sbot, getProfile)
  const countCommunities = CountCommunities(sbot)
  const countProfiles = CountProfiles(sbot)
  const countWhakapapaViews = CountWhakapapaViews(sbot)

  return {
    getInvitedPeople: p(getInvitedPeople),
    countCommunities: p(countCommunities),
    countProfiles: p(countProfiles),
    countWhakapapaViews: p(countWhakapapaViews)
  }
}
