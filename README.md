# GraphQL for Secure Scuttlebutt

Pataka GraphQL types and resolvers for Secure Scuttlebutt.
It provides primitives for loading data Whakapapa data summaries.

## Usage

`npm i -S @ssb-graphql/pataka`

## Example Usage

```js
const ahauServer = require('ahau-graphql-server')

const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const sbot = Server
  .use(require('ssb-profile')) // << required
  .use(require('ssb-invite')) // << required
  .call(null, config)

const server = await ahauServer({
  schema: [
    require('@ssb-graphql/main')(sbot)
    require('@ssb-graphql/profile')(sbot)
    require('@ssb-graphql/pataka')(sbot)
  ]
})
```

## Requirements

An `ssb-server` with the `ssb-profile` plugin:

## API

See `/src/typeDefs.js` for the most up to date details on what's offered by this module.

## Testing

run `npm test` to run tests

