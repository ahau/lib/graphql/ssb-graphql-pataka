const tape = require('tape')
const { promisify: p } = require('util')
const { replicate } = require('scuttle-testbot')

const TestBot = require('./test-bot')

tape('Whakapapa records', async t => {
  t.plan(5)
  const { ssb, apollo } = await TestBot()

  const result = await apollo.query({
    query: `{
      dataSummary {
        storyRecords
        artefactRecords
        profileRecords
        whakapapaRecords
        communityRecords
      }
    }`
  })
  t.error(result.errors, 'query should not return errors')
  t.equals(typeof result.data, 'object', 'result.data is an object')
  t.equals(
    typeof result.data.dataSummary.profileRecords,
    'number',
    'result.data.dataSummary.profileRecords is a number'
  )
  t.equals(
    typeof result.data.dataSummary.whakapapaRecords,
    'number',
    'result.data.dataSummary.whakapapaRecords is a number'
  )
  t.equals(
    typeof result.data.dataSummary.communityRecords,
    'number',
    'result.data.dataSummary.communityRecords is a number'
  )

  ssb.close()
})

tape('Invited people', async t => {
  t.plan(2)
  const { ssb, apollo } = await TestBot({ loadContext: 'pataka' })
  const { ssb: friend, context } = await TestBot({ loadContext: 'person', apollo: false })

  // when a person uses an invite code, we follow them
  // so simulate an invite accept by following another feedId
  await p(ssb.friends.follow)(friend.id, { state: true })
    .catch(t.error)

  await replicate({ from: friend, to: ssb })
  friend.close()

  const result = await apollo.query({
    query: `{
      invitedPeople {
        id
        preferredName
      }
    }`
  })

  t.error(result.errors, 'query should not return errors')
  t.deepEquals(
    result.data.invitedPeople
      .map(el => ({ ...el })),
    [{ id: context.public.profileId, preferredName: null }],
    'result.data is an object'
  )

  ssb.close()
})
